<center>

<img src="http://bws.org.uk/images/GSU-Icon.png">

# <u>A Simplified Gambas3 Update/Compile/Install Utility</u>

<h2>For Download/Update and install of the latest<br>
<a href="https://gitlab.com/gambas/gambas">Master or Stable Gambas3 on Gitlab.</a></h2>

## This software is WIP ('a work in progress')
Written by Bruce Steers.\
Email: [gsu@bws.org.uk](mailto:gsu@bws.org.uk)\
Version "1.1.13"

Written in Gambas basic\
(an older Gambas3 must already be installed)

</center>

<hr width=70%>

#### To upgrade your gambas basic to the latest version.

**Requirements:**
* An older version Gambas3 installed.
* gambas components gb.net.curl, gb.form.terminal, gb.settings, gb.markdown
* An internet connection to download latest gambas archive from GitLab
* The command 'unzip' to unpack the downloaded archive.
* bash shell, required for log file as shell commands use the **set -o pipefail** instruction bash provides.

**Getting the program**
* Get the whole source and make the exe with your existing Gambas IDE.\
    **$ git clone https://gitlab.com/bsteers4/gambassimpleupgrade.git** 
* Or simply download the gambas exe (you may need to set the files executable flag)\
    **[Click here to download the standalone gambas exe](https://gitlab.com/bsteers4/gambassimpleupgrade/-/raw/master/GambasSimpleUpgrade.gambas)**



<hr width=70%>

**Supported Linux distributions...**
* Ubuntu (various versions including dev hirsute 21)
* LinuxMint (as ubuntu)
* Debian (stable, unstable, testing)
* Raspbian
* Fedora (latest)
* Archlinux (Manjaro)
* OpenSuse (leap/tumbleweed)

<hr width=70%>

## How to use...
* Launch the application and it should auto-detect your linux version.

#### Package dependencies...
* All dependencies/Packages needed to compile gambas are auto-listed and can be auto-installed.

#### Gambas source code folder.
* You can download a new source or select an existing folder containing a gambas-master or gambas-stable dir.
* GitLab can be checked for new commit versions/commit message.

### Compiling / Installing...
* 'make install' password can be entered at start so you do not have to wait.

#### Uninstalling the compiled gambas version.
* It will uninstall any distribution package manager Gambas installation (avoids conflicts).
* Can run 'make uninstall' (removes compiled gambas installation)
* Can run 'make distclean' (restores source dir to original state for clean re-compile)

### Please report any bugs you find to [My Email Address](mailto:bsteers4@gmail.com)

<table border=1 cellspacing=10 cellpadding=10 align=center>
<tr><td align=center valign=center colspan=3>Intro<br><a href="http://bws.org.uk/images/GSU/GSU-Intro.png"><img width=800 src="http://bws.org.uk/images/GSU/GSU-Intro.png"></a></td></tr>
<tr><td align=center valign=center colspan=3>Disribution Selection<br><a href="http://bws.org.uk/images/GSU/GSU-Distros.png"><img width=800 src="http://bws.org.uk/images/GSU/GSU-Distros.png"></a></td></tr>
<tr><td align=center valign=center colspan=3>Source / Download Page<br><a href="http://bws.org.uk/images/GSU/GSU-Source.png"><img width=800 src="http://bws.org.uk/images/GSU/GSU-Source.png"></a></td></tr>
<tr><td align=center valign=center colspan=3>Downloading<br><a href="http://bws.org.uk/images/GSU/GSU-Download.png"><img width=800 src="http://bws.org.uk/images/GSU/GSU-Download.png"></a></td></tr>
<tr><td align=center valign=center colspan=3>Compile / Install Page.<br><a href="http://bws.org.uk/images/GSU/GSU-Compile.png"><img width=800 src="http://bws.org.uk/images/GSU/GSU-Compile.png"></a></td></tr>
<tr><td align=center valign=center colspan=3>Finished.<br><a href="http://bws.org.uk/images/GSU/GSU-Finished.png"><img width=800 src="http://bws.org.uk/images/GSU/GSU-Finished.png"></a></td></tr>
</table>
