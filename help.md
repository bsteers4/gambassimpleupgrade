
<h1 align=center> A Simple Gambas3 upgrade utility</h1>
<h4 align=center>Version "1.1.13"</h4>

<h2 align=center>Written by Bruce Steers<br></h2>
<p>
## How to use...

#### \* On the intro page click next to initialise and choose some options.
(An internet connection is required to download/update.)\

\* Your linux version should be detected at this point, If not then either something is wrong or you are using an unsupported Linux.\

<hr width=50%>

#### \* By default the source is downloaded to your home folder but you can change the path.

### \* Select Stable or Development branch to download.
(Or select a directory containing an existing gambas-master or gambas-stable folder)\

#### \* Click next.\
 \* If you are missing any packages you can install them here. If you have all packages then the package install page is skipped.

#### \* Then you are asked if you would like to download the archive and unpack it.\
\* Select no if you do not want to download and refresh the source folder and only want to install from a previously downloaded one.\

<hr width=50%>

#### \* Once on the compile page press 'Begin' to begin compilation/install.

\* Before compilation you can choose what to do when 'make install' asks for your password (in case you are not at the keyboard come this point),\

Either do nothing (sudo will ask for password and timeout if you are not around)\

Or pause (pops up a message pausing the process till the user clicks okay)\

Or enter password at the start where it is stored im memory and passed to sudo when asked.\
\
\* Right click the Begin button to have options to start compile from any point or to only run a single compilation command.\

<hr width=50%>

## That should be it.
<br>
## If all went well the new Gambas will be installed and ready to use.
